# Crawl
Basic crawler

Written for python 3.7.

Will work on python 3 for versions less than 3.7, but will have duplicate url scrapes (didn't explicitly import OrderedDict)
Will work on python 2, but needs some cleanup.

Has a hardcoded soft limit of 1000 url scraped at the moment.

No 3rd party deps.

## Usage

clone repo, go to repo root and :

```
$ python3 crawl.py <seed url>
```
