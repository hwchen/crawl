# Basic scraper

# It assumes that it's always scraping html; it will probably silently fail on json or binary, etc. because it expects the `href` field.
# It uses only std library; when convenient to use 3rd party libs, `requests` and `beautiful soup` would probably used for scraping.
# Error handling is not as robust as a prod app. There are many exceptions from urllib3 which are not covered in detail.
# Using 3.7 python for ordered dict. Could also import OrderedDict


import urllib.request
import re
import sys


def main():
    seed_url = sys.argv[1]
    limit = 1000 # could also be a cli arg

    # using 3.7's ordered dict as an ordered set
    url_repo = {seed_url: 0}

    # initialize working queue
    for url in scrape(seed_url):
        url_repo[url] = 0

    # if seed url fails or simply produces no additional urls, just exit
    if len(url_repo) == 1:
        print('Error: seed did not produce additional urls; exiting program')
        sys.exit(1)

    # With seed, begin the process of continuously scraping

    # The url_repo is walked one-by-one to get urls to scrape
    # The result of each url's scrape is also dumped into
    # url_repo.
    # process stopped when the last url's scrape adds enough
    # urls for the url_repo to exceed the limit
    idx = 0

    while len(url_repo) < limit:
        # this is slow because can't directly index into
        # dict. With 3rd party packages would choose an
        # impl that allowed direct indexing.
        url = list(url_repo.keys())[idx]

        print('scraping', url)
        for u in scrape(url):
            url_repo[u] = 0
        idx += 1
        print('repo length:', len(url_repo))

    # just printing for now. Could persist to disk, db, etc.
    print(list(url_repo.keys()))


# http is the urllib3 pool for making requests
# url is string of url to scrape
# returns a list of urls
def scrape(url):
    try:
        with urllib.request.urlopen(url) as r:
            body = r.read().decode('utf-8')

            # don't parse DOM, just get all url
            # which have `href` prefix and are `http`
            #
            # regex here :)
            # Normally I'd use beautiful soup or something
            # for scraping.

            # TODO Note: this regex only handles one particular
            # quoting configuration. single quotes and no
            # quotes are also allowed for attribute value
            pattern = re.compile('href="http.+?"')
            return list(map(lambda s: s[6:-1], pattern.findall(body)))
    except:
        # would handle more thoroughly in prod;
        # this is just a stub for further error handling
        # normally would reraise the exception too
        print('Could not scrape', url)
        return []


if __name__ == "__main__":
    main()
